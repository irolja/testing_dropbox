# -*- coding: utf-8 -*-
import dropbox
import requests
import re
import os
import settings


class DropboxFileUploader(object):
    def __init__(self):
        self.access_token = self._authorize()

    def _save_token(self, filename, dict1):
        '''
        Change value for given key
        '''
        RE = '(('+'|'.join(dict1.keys())+')\s*=)[^\r\n]*?(\r?\n|\r)'
        pat = re.compile(RE)

        def set_value(mat, dic=dict1):
            return dic[mat.group(2)].join(mat.group(1, 3))

        with open(filename, 'rb') as file:
            content = file.read()

        with open(filename, 'wb') as file:
            file.write(pat.sub(set_value, content))

    def _authorize(self):
        if settings.AUTH_CODE:
            try:
                dropbox.Dropbox(settings.AUTH_CODE).users_get_current_account()
            except Exception, e:
                print('Error: %s' % (e,))
            else:
                return settings.AUTH_CODE
        app_key = settings.APP_KEY
        app_secret = settings.APP_SECRET
        auth_flow = dropbox.DropboxOAuth2FlowNoRedirect(app_key, app_secret)
        authorize_url = auth_flow.start()
        print "1. Go to: " + authorize_url
        print "2. Click \"Allow\" (you might have to log in first)."
        print "Test username: rolja.test@gmail.com"
        print "Password: 1234qwe"
        print "3. Copy the authorization code."
        auth_code = raw_input("Enter the authorization code here: ").strip()
        try:
            access_token, user_id = auth_flow.finish(auth_code)
        except Exception, e:
            print('Error: %s' % (e,))
            return ''
        new_auth_code = {'AUTH_CODE': '\"' + access_token + '\"'}
        self._save_token('settings.py', new_auth_code)
        return access_token

    def _check_file(self, url):
        file_head = requests.head(url)
        redirects = 0
        while (redirects < 5 and (
                file_head.is_redirect or file_head.is_permanent_redirect)):
            redirects += 1
            url = file_head.headers['Location']
            file_head = requests.head(url)
        if file_head.status_code == requests.codes.ok:
            file_size = int(file_head.headers['Content-Length'])
        else:
            return {'error_description': 'File does not exists.'}
        space = dropbox.Dropbox(self.access_token).users_get_space_usage()
        avaliable = space.allocation.get_individual().allocated
        try:
            avaliable += space.allocation.get_team()
        except AttributeError:
            pass
        avaliable = avaliable - space.used
        if file_size > avaliable:
            return {'error_description': 'Not enough free space.'
                    'Available %s, file size %s' % (avaliable, file_size)}
        return url

    def upload_url_direct(self, url):
        if not self.access_token:
            return {'error_description': 'Wrong authorization code'}
        url = self._check_file(url)
        if isinstance(url, dict):
            return url
        headers = {'Authorization': 'Bearer ' + self.access_token}
        file_url = {'url': url}
        file_name = os.path.basename(url)
        path_url = ('https://api.dropboxapi.com/1/save_url/auto/%s') % (
            file_name)
        send_file = requests.post(
            path_url, data=file_url, headers=headers, allow_redirects=False)
        return send_file.json()

    def check_url_direct_status(self, job_id):
        access_token = self.access_token
        if not access_token:
            return {'error_description': 'Wrong authorization code'}
        headers = {'Authorization': 'Bearer ' + access_token}
        check_url = 'https://api.dropbox.com/1/save_url_job/' + job_id
        response = requests.get(check_url, headers=headers)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            return response

    def _download_file(self, url):
        local_filename = os.path.basename(url)
        response = requests.get(url, stream=True)
        with open(local_filename, 'wb') as file:
            for chunk in response.iter_content(chunk_size=10485760):
                file.write(chunk)
        return local_filename

    def _get_chunks(self, file_size):
        chunk_start = 0
        chunk_size = 10485760
        while chunk_start + chunk_size < file_size:
            yield(chunk_start, chunk_size)
            chunk_start += chunk_size
        final_chunk_size = file_size - chunk_start
        yield(chunk_start, final_chunk_size)

    def _upload_file_chunked(self, file_path):
        dbx = dropbox.Dropbox(self.access_token)
        with open(file_path) as file_:
            file_size = os.path.getsize(file_path)
            file_name = os.path.basename(file_path)
            progress = 0
            for chunk_start, chunk_size in self._get_chunks(file_size):
                file_chunk = file_.read(chunk_size)
                if chunk_start == 0:
                    start = dbx.files_upload_session_start(file_chunk)
                elif chunk_start + chunk_size < file_size:
                    dbx.files_upload_session_append(
                        file_chunk,
                        start.session_id,
                        progress)
                else:
                    cursor = dropbox.files.UploadSessionCursor(
                        session_id=start.session_id, offset=progress)
                    commit = dropbox.files.CommitInfo(
                        '/' + file_name,
                        mode=dropbox.files.WriteMode('add'),
                        autorename=True)
                    finish = dbx.files_upload_session_finish(
                        file_chunk,
                        cursor,
                        commit
                        )
                progress += len(file_chunk)
            return finish

    def upload_url_disk(self, url):
        if not self.access_token:
            return {'error_description': 'Wrong authorization code'}
        file_path = self._check_file(url)
        if isinstance(file_path, dict):
            return file_path
        file_path = self._download_file(file_path)
        if os.stat(file_path).st_size < 157286400:
            dp_path = '/%s' % os.path.basename(file_path)
            dbx = dropbox.Dropbox(self.access_token)
            file = open(file_path, 'r')
            response = dbx.files_upload(
                file,
                dp_path,
                mode=dropbox.files.WriteMode('add'),
                autorename=True)
        else:
            response = self._upload_file_chunked(file_path)
        os.remove(file_path)
        return response

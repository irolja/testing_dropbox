# -*- coding: utf-8 -*-
import unittest
import dropbox
from file_upload import DropboxFileUploader


class TestDropboxFileUploader(unittest.TestCase):
    def test_upload_url_direct(self):
        uploader = DropboxFileUploader()
        response = uploader.upload_url_direct('http://www.ex.ua/load/235412237')
        status = ['PENDING', 'DOWNLOADING', 'COMPLETE']
        self.assertTrue(response['status'] in status)

        check = uploader.check_url_direct_status(response['job'])
        self.assertTrue(check['status'] in status)

    def test_upload_url_disk(self):
        uploader = DropboxFileUploader()
        resp = uploader.upload_url_disk('http://www.ex.ua/load/235412237')
        self.assertTrue(isinstance(resp, dropbox.files.Metadata))

if __name__ == '__main__':
    unittest.main()
